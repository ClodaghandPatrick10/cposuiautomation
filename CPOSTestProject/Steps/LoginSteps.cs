﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using CPOSTestProject.Pages;
using CPOSAutomationFramework.Settings;

namespace CPOSTestProject.Steps
{
    [Binding]

    public class LoginSteps
    { 

    private DriverHelper _driverHelper;
    HomePage homePage;
    LoginPage loginPage;

    public LoginSteps(DriverHelper driverHelper)
    {
        _driverHelper = driverHelper;
        homePage = new HomePage(_driverHelper.Driver);
        loginPage = new LoginPage(_driverHelper.Driver);
    }

        [Given(@"I navigate to application")]
        public void GivenINavigateToApplication()
        {
            _driverHelper.Driver.Navigate().GoToUrl("https://qpqa.campuscloud.io/admin.html?tenant=quadpointadmin");
        }
        
        [Given(@"I click the Sign in via Transact AD")]
        public void GivenIClickTheSignInViaTransactAD()
        {
            homePage.ClickSignInViaTransactAD();
        }
        
        [Given(@"I enter username and password")]
        public void GivenIEnterUsernameAndPassword(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            loginPage.EnterUserNameAndPassword(data.UserName, data.Password);
        }
        
        [Given(@"I click login")]
        public void GivenIClickLogin()
        {
            loginPage.ClickLogin();
        }
     
        [Then(@"I should see user logged in to the application")]
        public void ThenIShouldSeeUserLoggedInToTheApplication()
        {
            Assert.That(homePage.IsLogOffExist(), Is.True, "Log off button did not displayed");
        }



    }
}

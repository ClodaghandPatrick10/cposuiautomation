﻿Feature: Login
Check if login functionality works

Scenario: Sign in via Transact AD
Given I navigate to application
And I click the Sign in via Transact AD
And I enter username and password
| UserName                    | Password             |
| nicola.kehoe@campuscloud.io | ClodaghandPatrick10! |
And I click login
#Then I should see user logged into the application


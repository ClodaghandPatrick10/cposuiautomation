﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
//using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace CPOSTestProject.Pages
{
    class HomePage
    {
        private IWebDriver Driver;

        public HomePage(IWebDriver driver)
        {
            Driver = driver;
        }

        IWebElement lnkSignInViaTransactADLogin => Driver.FindElement(By.XPath("//span[@id='button-1032-btnInnerEl']"));

        IWebElement lnkLogOff => Driver.FindElement(By.LinkText("Log off"));

        public void ClickSignInViaTransactAD()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            IWebElement lnkSignInViaTransactADLogin = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[@id='button-1032-btnInnerEl']")));
            lnkSignInViaTransactADLogin.Click();
        }

        public bool IsLogOffExist() => lnkLogOff.Displayed;
    }
}


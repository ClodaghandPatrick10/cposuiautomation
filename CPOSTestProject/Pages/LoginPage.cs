﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace CPOSTestProject.Pages
{
    public class LoginPage
    {
        private IWebDriver Driver;

        public LoginPage(IWebDriver driver)
        {
            Driver = driver;
        }

        IWebElement txtUserName => Driver.FindElement(By.XPath("//input[@id='i0116']"));
        IWebElement txtPassword => Driver.FindElement(By.XPath("//input[@id='i0118']"));
        IWebElement btnNext => Driver.FindElement(By.XPath("//input[@id='idSIButton9']"));
        IWebElement btnLogin => Driver.FindElement(By.XPath("//input[@id='idSIButton9']"));
        IWebElement btnYes => Driver.FindElement(By.XPath("//input[@id='idSIButton9']"));


        public void EnterUserNameAndPassword(string userName, string password)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            IWebElement txtUserName = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='i0116']")));
            txtUserName.SendKeys(userName);
            btnNext.Click();
            IWebElement txtPassword = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='i0118']")));
            txtPassword.SendKeys(password);
        }

        public void ClickLogin()
        {
            btnLogin.Click();
            btnYes.Click();
        }
    }
}


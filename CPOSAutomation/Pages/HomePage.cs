﻿using OpenQA.Selenium;
using System;
using CPOSAutomationFramework.Extensions;
using System.Collections.Generic;
using System.Text;
//using SeleniumExtras.WaitHelpers;
using CPOSAutomationFramework.Settings;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;


namespace CPOSAutomationFramework.Pages
{
   public class HomePage
    {
          private IWebDriver Driver;

          public HomePage(IWebDriver driver)
        {
            this.Driver = driver;
        }
 
        IWebElement lnkSignInViaTransactADLogin => Driver.FindElement(By.XPath("//span[@id='button-1032-btnInnerEl']"));

        IWebElement lnkSignOff => Driver.FindElement(By.XPath("//span[@id='button-1152-btnInnerEl']"));

        public bool lnkSignOffExist()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            try
            {
                IWebElement lnkSignOff = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[@id='button-1152-btnInnerEl']")));
            if (lnkSignOff.Displayed)
                {
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }
    }
}
     


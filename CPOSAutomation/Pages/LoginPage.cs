﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.UI;
using CPOSAutomationFramework.Extensions;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using CPOSAutomationFramework.Settings;


namespace CPOSAutomationFramework.Pages
{
    public class LoginPage
    {
        private IWebDriver Driver;
        public LoginPage(IWebDriver driver)
        {
            this.Driver = driver;
        }

        IWebElement txtADUserName => Driver.FindElement(By.XPath("//input[@id='i0116']"));
        IWebElement txtADPassword => Driver.FindElement(By.XPath("//input[@id='i0118']"));
        IWebElement txtUserName => Driver.FindElement(By.XPath("//input[@id='UserLogin.UserName-inputEl']"));
        IWebElement txtPassword => Driver.FindElement(By.XPath("//input[@id='UserLogin.password-inputEl']"));
        IWebElement txtPws => Driver.FindElement(By.XPath("//input[@id='textfield-1036-inputEl']"));
        IWebElement btnNext => Driver.FindElement(By.XPath("//input[@id='idSIButton9']"));
        IWebElement btnLogin => Driver.FindElement(By.XPath("//input[@id='idSIButton9']"));
        IWebElement btnYes => Driver.FindElement(By.XPath("//input[@id='idSIButton9']"));
        IWebElement btnSignInViaTransactADLogin => DriverContext.Driver.FindElement(By.XPath("//span[@id='button-1032-btnInnerEl']"));
        IWebElement btnSignLogin => Driver.FindElement(By.XPath("//span[@id='button-1031-btnInnerEl']"));
        IWebElement btnSignIn => Driver.FindElement(By.XPath("//span[@id='UserLogin.ButtonLogin-btnInnerEl']"));

        public void ClickSignInViaTransactAD()
        {
           WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            IWebElement btnSignInViaTransactADLogin = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[@id='button-1032-btnInnerEl']")));
            btnSignInViaTransactADLogin.Click();
        }

        public void ClickSignIn()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            IWebElement btnSignLogin = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[@id='button-1031-btnInnerEl']")));
            btnSignLogin.Click();
        }


        public void EnterADUserNameAndPassword(string userName, string password)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            IWebElement txtADUserName = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='i0116']")));
            txtADUserName.SendKeys(userName);
            btnNext.Click();
            IWebElement txtADPassword = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='i0118']")));
            txtADPassword.SendKeys(password);
        }
        public void EnterUserNameAndPassword(string userName, string password)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            IWebElement txtUserName = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='UserLogin.UserName-inputEl']")));
            txtUserName.SendKeys(userName);
            IWebElement txtPws = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='textfield-1036-inputEl']")));
            txtPws.SendKeys(password);
            IWebElement btnSignIn = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[@id='UserLogin.ButtonLogin-btnInnerEl']"))); ;
            btnSignIn.Click();
        }

            public void EnterUserNameAndPasswordAndClickSignin(string userName, string password)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            IWebElement txtUserName = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='UserLogin.UserName-inputEl']")));
            txtUserName.SendKeys(userName);
            IWebElement txtPassword = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='UserLogin.password-inputEl']")));
            txtPassword.SendKeys(password);
            IWebElement btnSignIn = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[@id='UserLogin.ButtonLogin-btnInnerEl']"))); ;
            btnSignIn.Click();
        }

        public void ClickLogin()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            btnLogin.Click();
            IWebElement btnYes = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='idSIButton9']")));
            btnYes.Click();
        }
    }
}


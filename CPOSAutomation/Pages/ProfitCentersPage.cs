﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Threading;
using NUnit.Framework;
using CPOSAutomationFramework.Extensions;
using CPOSAutomationFramework.Settings;

namespace CPOSAutomationFramework.Pages
{
    class ProfitCentersPage
    {
        private IWebDriver Driver;
        private string name;


            public ProfitCentersPage(IWebDriver driver)

        {
            this.Driver = driver;
        }
        IWebElement btnProfitCenters => Driver.FindElement(By.XPath("//div[@id='qpleftmenupanel-1025_header-title-textEl']"));
        IWebElement btnProfitCentersunderProfitCenters => Driver.FindElement(By.Id("button-1028-btnInnerEl"));
        public void ClickProfitCenters()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            IWebElement btnProfitCenters = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='qpleftmenupanel-1025_header-title-textEl']")));
            btnProfitCenters.Click();
        }

       public void ClickProfitCentersUnderProfitCenters()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            IWebElement btnProfitCentersunderProfitCenters = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("button-1028-btnInnerEl")));
            btnProfitCentersunderProfitCenters.Click();
        }
    }
}
﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Threading;
using NUnit.Framework;
using CPOSAutomationFramework.Extensions;
using CPOSAutomationFramework.Settings;


namespace CPOSAutomationFramework.Pages
{
    class DepartmentsPage
    {
        private IWebDriver Driver;
        private string name;
        private string remote;

             public DepartmentsPage(IWebDriver driver)
 
        {
            this.Driver = driver;
        }

        IWebElement btnProfitCenters => Driver.FindElement(By.XPath("//div[@id='qpleftmenupanel-1025_header-title-textEl']"));
        IWebElement btnDepartments => Driver.FindElement(By.XPath("//span[@id='button-1027-btnInnerEl']"));
        IWebElement btnNewDepartment => Driver.FindElement(By.XPath("//div[@id='qpsheadertool-1224-toolEl'][1]"));
        IWebElement txtName => Driver.FindElement(By.Id("textfield-1447-inputEl"));
        IWebElement txtDescription => Driver.FindElement(By.Id("textfield-1448-inputEl"));
        IWebElement txtPhone => Driver.FindElement(By.XPath("//input[@id='textfield-1449-inputEl']"));
        IWebElement txtFax => Driver.FindElement(By.XPath("//input[@id='textfield-1450-inputEl']"));
        IWebElement txtEmail => Driver.FindElement(By.XPath("//input[@id='textfield-1451-inputEl']"));
        IWebElement txtContact => Driver.FindElement(By.XPath("//input[@id='textfield-1452-inputEl']"));
        IWebElement txtAddress => Driver.FindElement(By.XPath("//textarea[@id='textareafield-1454-inputEl']"));
        IWebElement txtFirstItemNumber => Driver.FindElement(By.XPath("//input[@id='qpnumberfield-1456-inputEl']"));
        IWebElement txtLastItemNumber => Driver.FindElement(By.XPath("//input[@id='qpnumberfield-1457-inputEl']"));
        IWebElement btnSaveAndClose => Driver.FindElement(By.XPath("//span[@id='qpbuttonicon-1475-btnInnerEl']"));
        IWebElement txtTestDepartment => Driver.FindElement(By.XPath("//table[@id='tableview-1197-record-1866']/tbody/tr/td[1]/div[contains(text(),'Test Department')]"));
        IWebElement btnProperties => Driver.FindElement(By.XPath("//div[@id='qpsheadertool-1223-toolEl'][1]"));
        IWebElement txtPropertiesName => Driver.FindElement(By.XPath("(//*[text()='Name:']/following::input)[1]"));
        IWebElement txtPropertiesDescription => Driver.FindElement(By.Id("textfield-1684-inputEl"));
        IWebElement txtPropertiesPhone => Driver.FindElement(By.Id("textfield-1685-inputEl"));
        IWebElement txtPropertiesFax => Driver.FindElement(By.Id("textfield-1686-inputEl"));
        IWebElement txtPropertiesEmail => Driver.FindElement(By.Id("textfield-1687-inputEl"));
        IWebElement txtPropertiesContact => Driver.FindElement(By.Id("textfield-1688-inputEl"));
        IWebElement txtPropertiesAddress => Driver.FindElement(By.Id("textareafield-1690-inputEl"));
        IWebElement txtPropertiesFirstItemNumber => Driver.FindElement(By.Id("qpnumberfield-1692-inputEl"));
        IWebElement txtPropertiesLastItemNumber => Driver.FindElement(By.Id("qpnumberfield-1693-inputEl"));
        IWebElement txtDepartmentName => Driver.FindElement(By.XPath("//table[@id='tableview-1197-record-1852']/tbody/tr/td[1]/div[contains(text(),'" + name + "')]"));
        IWebElement btnDeleteDepartment => Driver.FindElement(By.XPath("//div[@id='qpsheadertool-1225-toolEl'][1]"));
        IWebElement btnAlertYes => Driver.FindElement(By.Id("button-1006-btnInnerEl"));
        IWebElement txtSearchFor => Driver.FindElement(By.Id("textfield-1180-inputEl"));
        IWebElement ddArrowRemote => Driver.FindElement(By.Id("qpbasefixedcombo-1453-trigger-picker"));
        // IWebElement ddArrowRemoteOption => Driver.FindElement(By.XPath("//ul[@id='boundlist-1503-listEl']/li[contains(text(),'Yes')]"));
        IWebElement ddArrowRemoteOption => Driver.FindElement(By.XPath("//*[text()='Remote?']/following::li[2]"));
        IWebElement ddSelectedRemoteOption => Driver.FindElement(By.XPath("//ul[@id='boundlist-1739-listEl']/li[contains(@class,'selected')]"));
        IWebElement SearchDepartmentName => Driver.FindElement(By.XPath("//div[@id='ext-element-23']/div/table/tbody/tr/td/div[contains(text(),'" + name + "')]"));
        IWebElement btnClose => Driver.FindElement(By.Id("qpbuttonicon-1712-btnInnerEl"));
        public void ClickDepartments()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
            IWebElement btnDepartments = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[@id='button-1027-btnInnerEl']")));
            btnDepartments.Click();
        }

        public void ClickNewDepartment()
        {
            Thread.Sleep(5000);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
            IWebElement btnNewDepartment = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='qpsheadertool-1224-toolEl'][1]")));
            btnNewDepartment.Click();
        }
        public void ClickProperties()
        {
            Thread.Sleep(5000);
            btnProperties.Click();
        }

        public void ClickClose()
        {
            Thread.Sleep(3000);
            btnClose.Click();
        }

        public void SelectDepartmentName(string name)
        {
            Thread.Sleep(5000);
            txtDepartmentName.Click();
        }

        public void ClickDeleteDepartment()
        {
            btnDeleteDepartment.Click();
        }
        public void ClickYesOnAlert()
        {
            btnAlertYes.Click();
        }
        public void SaveAndClose()
        {
            btnSaveAndClose.Click();
        }
        public void AddDepartmentDetails(string name, string phone, string fax, string email, string contact,string remote, string address, string firstItemNumber, string lastItemNumber)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            IWebElement txtName = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("textfield-1447-inputEl")));
            txtName.SendKeys(name);
            IWebElement txtPhone = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='textfield-1449-inputEl']")));
            txtPhone.SendKeys(phone);
            IWebElement txtfax = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='textfield-1450-inputEl']")));
            txtfax.SendKeys(fax);
            IWebElement txtEmail = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='textfield-1451-inputEl']")));
            txtEmail.SendKeys(email);
            IWebElement txtContact = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='textfield-1452-inputEl']")));
            txtContact.SendKeys(contact);
            ddArrowRemote.Click();
            ddArrowRemoteOption.Click();
            IWebElement txtAddress = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//textarea[@id='textareafield-1454-inputEl']")));
            txtAddress.SendKeys(address);
            IWebElement txtFirstItemNumber = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='qpnumberfield-1456-inputEl']")));
            txtFirstItemNumber.SendKeys(firstItemNumber);
            IWebElement txtLastItemNumber = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='qpnumberfield-1457-inputEl']")));
            txtLastItemNumber.SendKeys(lastItemNumber);
        }

        public void SearchforDepartmentName(string name)
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            IWebElement txtSearchFor = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("textfield-1180-inputEl")));
            txtSearchFor.SendKeys(name);
            Thread.Sleep(2000);
        }
        public bool TestDepartmentExist(string name)
        {

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            try
            {
                IWebElement SearchDepartmentName = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(//div[text()='" + name + "'])[1]")));
                if (SearchDepartmentName.Displayed)
                {
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public void AssertDepartmentDetails(string name, string phone, string fax, string email, string contact, string address, string firstItemNumber, string lastItemNumber)
        {
            Assert.That(name, Is.EqualTo(txtPropertiesName.GetAttribute("value")), "Name is not correct");
            Assert.That(name, Is.EqualTo(txtPropertiesDescription.GetAttribute("value")), "Description is not correct");
            Assert.That(phone, Is.EqualTo(txtPropertiesPhone.GetAttribute("value")), "Phone is not correct");
            Assert.That(fax, Is.EqualTo(txtPropertiesFax.GetAttribute("value")), "Fax is not correct");
            Assert.That(email, Is.EqualTo(txtPropertiesEmail.GetAttribute("value")), "Email is not correct");
            Assert.That(contact, Is.EqualTo(txtPropertiesContact.GetAttribute("value")), "Contact is not correct");
            Assert.That(address, Is.EqualTo(txtPropertiesAddress.GetAttribute("value")), "Address is not correct");
            Assert.That(firstItemNumber, Is.EqualTo(txtPropertiesFirstItemNumber.GetAttribute("value")), "First Item number is not correct");
            Assert.That(lastItemNumber, Is.EqualTo(txtPropertiesLastItemNumber.GetAttribute("value")), "Last Item number is not correct");
        }


    }
}
﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Threading;
using NUnit.Framework;
using CPOSAutomationFramework.Extensions;
using CPOSAutomationFramework.Settings;
using OpenQA.Selenium.Interactions;


namespace CPOSAutomationFramework.Pages
{
    class ImportExportPage
    {
        private IWebDriver Driver;


        public ImportExportPage(IWebDriver driver)

        {
            this.Driver = driver;
        }

        IWebElement btnImportExportData => Driver.FindElement(By.XPath("//div[text()='Import/Export Data']"));
        IWebElement btnExportData => Driver.FindElement(By.XPath("//span[text()='Export Data']"));
        IWebElement btnExportArrowcollapse => Driver.FindElement(By.Id("qpbasegrid-1179-splitter-collapseEl"));
        IWebElement btnTransactionData => Driver.FindElement(By.XPath("//div[text()='Transaction Data'])[1]"));
        IWebElement btnNewTransactionData => Driver.FindElement(By.XPath("//div[text()='New']"));
        IWebElement ddSearchOption => Driver.FindElement(By.XPath("(//*[text()='Search Option:']/following::input)[1]"));
        IWebElement txtExportName => Driver.FindElement(By.XPath("(//*[text()='Export Name:']/following::input)[1]"));
        IWebElement btnProfitCentersAddAll => Driver.FindElement(By.XPath("//*[text()='Profit Center(s)']/following::a[2]"));
        IWebElement btnExportColumnsAddAll => Driver.FindElement(By.XPath("//*[text()='Select Columns to Export']/following::a[2]"));
        IWebElement btnTransactionTypesAddAll => Driver.FindElement(By.XPath("//*[text()='Transaction type(s)']/following::a[2]"));
        IWebElement btnRunExport => Driver.FindElement(By.XPath("(//*[text()='Run Export']"));
        IWebElement btnSearchIcon => Driver.FindElement(By.XPath("//div[@class='x-tool-img x-tool-search']"));
        IWebElement txtLastRunDate => Driver.FindElement(By.XPath("(//table/tbody/tr/td[3])[2]"));
        IWebElement txtLastRunDateExportResults => Driver.FindElement(By.XPath("//td[@class='x-grid-cell x-grid-td x-grid-cell-qpdatetimecolumn-1658']"));
        IWebElement txtFileNameExportResults => Driver.FindElement(By.XPath("//td[@class='x-grid-cell x-grid-td x-grid-cell-gridcolumn-1657 x-grid-cell-first']"));
        IWebElement btnProperties => Driver.FindElement(By.XPath("//div[text()='Properties']"));
        IWebElement btnExportResults => Driver.FindElement(By.XPath("//*[text()='Export Results']"));
        IWebElement btnCloseExportWindow => Driver.FindElement(By.XPath("//*[text()='Close']"));
        IWebElement btnDeleteExport => Driver.FindElement(By.XPath("//div[text()='Delete']"));
        IWebElement btnYesConfirmation => Driver.FindElement(By.XPath("(//span[text()='Yes'])[1]"));
        IWebElement ddRunAndDDOptionsArrow => Driver.FindElement(By.XPath("//*[text()='Run and Export Options']/following::div[1]"));
        IWebElement dropdownValue => Driver.FindElement(By.XPath("//*[text()='Run Option:']/following::li[2]"));
        public void ClickImportExportData()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnImportExportData = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//div[text()='Import/Export Data']")));
            btnImportExportData.Click();
        }
        public void ClickExportData()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnExportData = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[text()='Export Data']")));
            btnExportData.Click();
            btnExportArrowcollapse.Click();
        }
        public void ClickTransactionData()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnTransactionData = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(//div[text()='Transaction Data'])[1]")));
            btnTransactionData.Click();
        }

        public void ClickNewTransactionData()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnNewTransactionData = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//div[text()='New']")));
            btnNewTransactionData.Click();
        }

        public void ScrollDownToSearchOptions()
        {
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("arguments[0].scrollIntoView(true);", ddSearchOption);
        }

        public void AddExportName(string name)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement txtExportName = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(//*[text()='Export Name:']/following::input)[1]")));
            txtExportName.SendKeys(name);
        }
        public void AddExportColumns()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnExportColumnsAddAll = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[text()='Select Columns to Export']/following::a[2]")));
            btnExportColumnsAddAll.Click();
        }
        public void AddProfitCenters()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnProfitCentersAddAll = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[text()='Profit Center(s)']/following::a[2]")));
            btnProfitCentersAddAll.Click();
        }

        public void AddTransactionTypes()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnTransactionTypesAddAll = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[text()='Transaction type(s)']/following::a[2]")));
            btnTransactionTypesAddAll.Click();
        }

        public void AddSearchOption()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement ddSearchOption = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(//*[text()='Search Option:']/following::input)[1]")));
            ddSearchOption.Click();
            IWebElement dropdownValue = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[text()='Search Option:']/following::li[2]")));
            dropdownValue.Click();
        }

        public void ClickRunExport()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnRunExport = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[text()='Run Export']")));
            btnRunExport.Click();
        }
        public void ClickSearchIcon()
        {
            Thread.Sleep(4000);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement btnSearchIcon = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//div[@class='x-tool-img x-tool-search']")));
            btnSearchIcon.Click();
        }

        public void SearchExportName(string name)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement txtSearchForValue = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(//*[text()='Search for']/following::input)[1]")));
            txtSearchForValue.SendKeys(name);
            Thread.Sleep(2000);
        }

        public void LastRunDateIsToday()
        {
            var todaysDate = DateTime.Now.ToString("yyyy-MM-dd");
            var lastRunDate = txtLastRunDate.Text.Split(" ")[0];
            Assert.That(lastRunDate, Is.EqualTo(todaysDate), "Last Run Date is Not Correct");
        }

        public void ClickProperties()
        {
            btnProperties.Click();
        }

        public void ClickExportResults()
        {
            Thread.Sleep(2000);
            btnExportResults.Click();
        }
        public void ExportResultsDisplaysName(string name)
        {
            Thread.Sleep(2000);
            var exportFileName = txtFileNameExportResults.Text;
            Assert.That(exportFileName, Contains.Substring(name),"Export File Name is Not Correct");
        }
        public void ExportResultsLastRunDateIsToday()
        {
            var todaysDate = DateTime.Now.ToString("yyyy-MM-dd");
            var lastRunDateExportResults = txtLastRunDateExportResults.Text.Split(" ")[0];
            Assert.That(lastRunDateExportResults, Is.EqualTo(todaysDate), "Export Report Last Run Date is Not Correct");
        }

        public void ClickCloseExportWindow()
        {
            btnCloseExportWindow.Click();
        }
        public void ClickDelteExport()
        {
            Thread.Sleep(1000);
            btnDeleteExport.Click();
            btnYesConfirmation.Click();
        }
        public void SelectRunAndDDOptionsArrow()
        {
            ddRunAndDDOptionsArrow.Click(); 
        }
        public void SelectRunOptionSchedule()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement ddRunOption = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(//*[text()='Run Option:']/following::input)[1]")));
            ddRunOption.Click();
            Thread.Sleep(1000);
            //IWebElement dropdownValue = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(//*[text()='Run Option:']/following::li)[2]")));
            dropdownValue.Click();
        }
    }

}

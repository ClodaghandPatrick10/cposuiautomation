﻿using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using NUnit.Framework;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;
using CPOSAutomationFramework.Settings;
using CPOSAutomationFramework.Helpers;

//[assembly: Parallelizable(ParallelScope.Fixtures)]

namespace CPOSAutomationFramework.Hooks
{
    [Binding]

    public class Hooks
    {

          private DriverHelper _driverHelper;
          public Hooks(DriverHelper driverHelper) => _driverHelper = driverHelper;

        [BeforeScenario]
        public void BeforeScenario()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("start-maximized");
            option.AddArguments("--disable-gpu");
            //option.AddArguments("--headless");
            new DriverManager().SetUpDriver(new ChromeConfig());
            _driverHelper.Driver = new ChromeDriver(option);

            //Set all the settings for framework
            ConfigReader.SetFrameworkSettings();
            TestSettings.ApplicationCon = TestSettings.ApplicationCon.DBConnect(TestSettings.AppConnectionString);
        }


            [AfterScenario]
        public void AfterScenario()
        {
            _driverHelper.Driver.Quit();
        }
    }
}



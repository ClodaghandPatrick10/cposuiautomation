﻿@ignore
Feature: AddAndDeleteNewProfitCenterInWebOffice
Background: 
	Given I navigate to application
And I enter username and password and click signin
| UserName | Password  |
| barry   | Limerick!4 |

@Regression
Scenario: Add A New Profit Center In Web Office
	Given I click Profit Centers
And I click on Profit Centers under the hierarchy of Profit Centers 
#And I click New on Profit Centers
And I fill in the following details
| Name                | Description                               | Phone           | Fax             | Email         | Contact    | Address         | FirstItemNumber | LastItemNumber |
| Test Department 123 | Added Test Department for Test Automation | 0011-0123456789 | 0011-0123456789 | info@test.com | Joe Bloggs | Sundrive Avenue | 1004            | 1542           |                  
And I select Save and Close
	When I search for "Test Department 123"
	Then I can see the newly Added "Test Department 123" is added to Departments
And I click on Properties
And I can see the following information for the saved department
	| Name                | Description                               | Phone           | Fax             | Email         | Contact    | Address         | FirstItemNumber | LastItemNumber |
	| Test Department 123 | Added Test Department for Test Automation | 0011-0123456789 | 0011-0123456789 | info@test.com | Joe Bloggs | Sundrive Avenue | 1004            | 1542           |   



Scenario: Delete Newly added Profit Center from Local Web Office
	Given I click Profit Centers
And I click Departments
And I search for "Test Department 123"
And I click Delele
	When I click Yes on the Alert Are you sure you want to delete Department Test Department
	Then I can see the newly Added "Test Department 123" is deleted from Departments
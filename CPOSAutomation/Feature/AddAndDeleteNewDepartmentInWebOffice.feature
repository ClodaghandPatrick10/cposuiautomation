﻿Feature: AddAndDeleteNewDepartmentInWebOffice
Background: 
	Given I navigate to application
And I enter username and password and click signin
| UserName | Password   |
| barry    | Limerick!5 |
And I click Profit Centers
And I click Departments 

@Regression
Scenario: Add A New Department In Web Office
	Given I click New on Departments
And I fill in the following details
| Name                | Phone           | Fax             | Email         | Contact    | Remote | Address         | FirstItemNumber | LastItemNumber |
| Test Department 123 | 0011-0123456789 | 0011-0123456789 | info@test.com | Joe Bloggs | Yes    | Sundrive Avenue | 1010            | 1542           |               
And I select Save and Close
	When I search for "Test Department 123"
	Then I can see the newly Added "Test Department 123" is added to Departments
And I click on Properties
And I can see the following information for the saved department
	| Name                | Phone           | Fax             | Email         | Contact    | Address         | FirstItemNumber | LastItemNumber |
	| Test Department 123 | 0011-0123456789 | 0011-0123456789 | info@test.com | Joe Bloggs | Sundrive Avenue | 1010            | 1542           |

Scenario: Delete Newly added Department from Web Office
	#Given I Insert a Department into Departments 
	And I search for "Test Department 123"
And I click Delele
	When I click Yes on the Alert Are you sure you want to delete Department Test Department
	Then I can see the newly Added "Test Department 123" is deleted from Departments
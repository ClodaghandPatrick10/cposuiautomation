﻿Feature: ExportService
Background: 
	Given I navigate to application
And I enter username and password and click signin
| UserName | Password   |
| cposauto | Password!1 |
And I click Import Export Data
And I click Export Data
And I click Transaction Data
And I click New Transaction Export

Scenario: Export Transaction Data Run Now Successful 
	Given I add Export Transaction name "Export Transaction Run Now" to Export
And I add all Export Columns to Export
And I click Run Export 
	When I search for "Export Transaction Run Now" on the search panel
	Then I can see the newly added "Export Transaction Run Now" Last Run Date is Today
And I click on Properties and Export Results
And I can see my "Export Transaction Run Now" in Export Results with Todays date
And I delete my "Export Transaction Run Now" from Transaction Data Exports

@ignore
Scenario: Scheduled Transaction Export Created Successful 
	Given I add Export Transaction name "Scheduled Export Transaction" to Export
And I add all Export Columns to Export
And I select Schedule from the Run Option Schedule
And I add Number of days to expire "90" 
And I select "Comma Delimited" from the File Format
And I click Run Export 
	When I search for "Scheduled Export Transaction" on the search panel
	Then I can see the newly added "Scheduled Export Transaction" Last Run Date is Today
And I click on Properties and Export Results
And I can see my "Scheduled Export Transaction" in Export Results with Todays date
And I delete my "Scheduled Export Transaction" from Transaction Data Exports
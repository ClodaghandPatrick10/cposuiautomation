﻿Feature: Log into Cloud Admin Test User
Check if login functionality works

Scenario: Log into Cloud Admin Test User
Given I navigate to application "https://qpqa.campuscloud.io/admin.html?tenant=quadpointadmin"
And I click the "Sign in"
And I enter "test" username and password
| UserName | Password |
| admin    | f4na%TIC |
﻿@ignore
Feature: Log into Local Web Test User
Check if login functionality works

Scenario: Log into Local Web Test User
	Given I navigate to application
And I enter username and password and click signin
| UserName | Password  |
| nicola   | Tester99@ |
	Then I should see user logged into the application
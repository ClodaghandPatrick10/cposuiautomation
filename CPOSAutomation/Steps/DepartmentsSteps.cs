﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using CPOSAutomationFramework.Pages;
using CPOSAutomationFramework.Settings;
using CPOSAutomationFramework.Helpers;

namespace CPOSAutomationFramework.Steps
{
    [Binding]
    class DepartmentsSteps : BasePage
    {
        private DriverHelper _driverHelper;
        LoginPage loginPage;
        HomePage homePage;
        DepartmentsPage departmentsPage;
        public DepartmentsSteps(DriverHelper driverHelper)

        {
            _driverHelper = driverHelper;
            homePage = new HomePage(_driverHelper.Driver);
            loginPage = new LoginPage(_driverHelper.Driver);
            departmentsPage = new DepartmentsPage(_driverHelper.Driver);

        }
        [Given(@"I Insert a Department into Departments")]
        public void GivenIInsertADepartmentIntoDepartments()
        {
            string query = "insert into departments values(CPOSAutomationDepartment, Added Test Department for Test Automation, 0111-13245678, 0111-12345678, infor@test.com, joe blogs, 1, sundrive Avenue, 1004, 1005)";
            TestSettings.ApplicationCon.ExecuteQuery(query);
        }

        [Given(@"I click Departments")]
        public void GivenIClickDepartments()
        {
            departmentsPage.ClickDepartments();
        }

        [Given(@"I click New on Departments")]
        public void GivenIClickNewOnDepartments()
        {
            departmentsPage.ClickNewDepartment();
        }

        [Given(@"I fill in the following details")]
        public void GivenIFillInTheFollowingDetails(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            departmentsPage.AddDepartmentDetails(data.Name, data.Phone, data.Fax, data.Email, data.Contact, data.Remote, data.Address, data.FirstItemNumber.ToString(), data.LastItemNumber.ToString());
        }

        [Given(@"I Select ""(.*)""")]
        public void GivenISelect(string name)
        {
            departmentsPage.SelectDepartmentName(name);
        }

        [Given(@"I click Delele")]
        public void GivenIClickDelele()
        {
            departmentsPage.ClickDeleteDepartment();
        }


        [Given(@"I select Save and Close")]
        public void WhenISelectSaveAndClose()
        {
            departmentsPage.SaveAndClose();
        }

        [Given(@"I search for ""(.*)""")]
        [When(@"I search for ""(.*)""")]
        public void GivenISearchFor(string name)
        {
            departmentsPage.SearchforDepartmentName(name);
        }

        [When(@"I click Yes on the Alert Are you sure you want to delete Department Test Department")]
        public void WhenIClickYesOnTheAlertAreYouSureYouWantToDeleteDepartmentTestDepartment()
        {
            departmentsPage.ClickYesOnAlert();
        }


        [Then(@"I can see the newly Added ""(.*)"" is added to Departments")]
        public void ThenICanSeeTheNewlyAddedIsAddedToDepartments(string department)
        {
            Assert.That(departmentsPage.TestDepartmentExist(department), Is.True, "Test Department is not displayed");
        }

        [Then(@"I can see the newly Added ""(.*)"" is deleted from Departments")]
        public void ThenICanSeeTheNewlyAddedIsDeletedFromDepartments(string department)
        {
            Assert.That(departmentsPage.TestDepartmentExist(department), Is.False, "Test Department is displayed");
        }

        [Then(@"I click on Properties")]
        public void ThenIClickOnProperties()
        {
            departmentsPage.ClickProperties();
        }
        [Then(@"I Click Close")]
        public void ThenIClickClose()
        {
            departmentsPage.ClickClose();
        }


        [Then(@"I can see the following information for the saved department")]
        public void ThenICanSeeTheFollowingInformationForTheSavedDepartment(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            departmentsPage.AssertDepartmentDetails(data.Name, data.Phone, data.Fax, data.Email, data.Contact, data.Address, data.FirstItemNumber.ToString(), data.LastItemNumber.ToString());
        }


    }
}

﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using CPOSAutomationFramework.Pages;
using CPOSAutomationFramework.Settings;
namespace CPOSAutomationFramework.Steps
    {
     [Binding]
    public class ProfitCentersSteps
    {
       private DriverHelper _driverHelper;
        HomePage homePage;
        LoginPage loginPage;
        ProfitCentersPage profitCenterPage;
          public ProfitCentersSteps(DriverHelper driverHelper)
    
        {
            _driverHelper = driverHelper;
            homePage = new HomePage(_driverHelper.Driver);
            loginPage = new LoginPage(_driverHelper.Driver);
            profitCenterPage = new ProfitCentersPage(_driverHelper.Driver);

        }

        [Given(@"I click Profit Centers")]
        public void GivenIClickProfitCenters()
        {
            profitCenterPage.ClickProfitCenters();
        }


        [Given(@"I click on Profit Centers under the hierarchy of Profit Centers")]
        public void GivenIClickOnProfitCentersUnderTheHierarchyOfProfitCenters()
        {
            profitCenterPage.ClickProfitCentersUnderProfitCenters();
        }

    }
}

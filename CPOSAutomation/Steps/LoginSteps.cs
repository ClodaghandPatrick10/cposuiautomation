﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using CPOSAutomationFramework.Pages;
using CPOSAutomationFramework.Settings;

namespace CPOSAutomationFramework.Steps
{
    [Binding]

    public class LoginSteps
    {

       private DriverHelper _driverHelper;
        HomePage homePage;
        LoginPage loginPage;
             public LoginSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            homePage = new HomePage(_driverHelper.Driver);
            loginPage = new LoginPage(_driverHelper.Driver);

        }

        [Given(@"I navigate to application ""(.*)""")]
            public void GivenINavigateToApplication(string url)
            {

            _driverHelper.Driver.Navigate().GoToUrl(url);
        }

        [Given(@"I navigate to application")]
        public void GivenINavigateToApplication()
        {
            _driverHelper.Driver.Navigate().GoToUrl("https://qpqa.campuscloud.io/?tenant=Bicycle");
        }

        [Given(@"I click the ""(.*)""")]
            public void GivenIClickThe(string signinOption)
            {
                switch (signinOption)
                {
                    case "Sign in via Transact AD":
                        loginPage.ClickSignInViaTransactAD();
                        break;
                    case "Sign in":
                        loginPage.ClickSignIn();
                        break;

                }

            }


            [Given(@"I click the Sign in via Transact AD")]
            public void GivenIClickTheSignInViaTransactAD()
            {
                loginPage.ClickSignInViaTransactAD();
            }

            [Given(@"I click the Sign in button")]
            public void GivenIClickTheSignInButton()
            {
                loginPage.ClickSignIn();
            }

            [Given(@"I enter ""(.*)"" username and password")]
            public void GivenIEnterUsernameAndPassword(string user, Table table)
            {
                if (user == "test")
                {
                    dynamic data = table.CreateDynamicInstance();
                    loginPage.EnterUserNameAndPassword(data.UserName, data.Password);
                }
                else {
                    dynamic data = table.CreateDynamicInstance();
                    loginPage.EnterADUserNameAndPassword(data.UserName, data.Password);

                }
            }
            [Given(@"I enter username and password and click signin")]
            public void GivenIEnterUsernameAndPasswordAndClickSignin(Table table)
            {
                dynamic data = table.CreateDynamicInstance();
                loginPage.EnterUserNameAndPasswordAndClickSignin(data.UserName, data.Password);
            }


            [Given(@"I click login")]
            public void GivenIClickLogin()
            {
                loginPage.ClickLogin();
            }

            [Then(@"I should see user logged into the application")]
            public void ThenIShouldSeeUserLoggedIntoTheApplication()
            {
                Assert.That(homePage.lnkSignOffExist(), Is.True, "Log off button not displayed");
            }

        }
    }

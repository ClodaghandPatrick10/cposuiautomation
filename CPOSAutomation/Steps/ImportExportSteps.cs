﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using CPOSAutomationFramework.Pages;
using CPOSAutomationFramework.Settings;
using CPOSAutomationFramework.Helpers;
using RestSharp;
using Newtonsoft.Json.Linq;
using CPOSAutomationFramework.Model;

namespace CPOSAutomationFramework.Steps
{
    [Binding]
    class ImportExportSteps : BasePage
    {
        private DriverHelper _driverHelper;
        private APISettings _settings;
        LoginPage loginPage;
        HomePage homePage;
        ImportExportPage importExportPage;
        public ImportExportSteps(DriverHelper driverHelper)

        {
            _driverHelper = driverHelper;
            homePage = new HomePage(_driverHelper.Driver);
            loginPage = new LoginPage(_driverHelper.Driver);
            importExportPage = new ImportExportPage(_driverHelper.Driver);
        }
        [Given(@"I click Import Export Data")]
        public void GivenIClickImportExportData()
        {
            importExportPage.ClickImportExportData();
        }

        [Given(@"I click Export Data")]
        public void GivenIClickExportData()
        {
            importExportPage.ClickExportData();
        }

        [Given(@"I click Transaction Data")]
        public void GivenIClickTransactionData()
        {
            importExportPage.ClickTransactionData();
        }

        [Given(@"I click New Transaction Export")]
        public void GivenIClickNewTransactionExport()
        {
            importExportPage.ClickNewTransactionData();
        }

        [Given(@"I scroll down to Search Options")]
        public void GivenIScrollDownToSearchOptions()
        {
            importExportPage.ScrollDownToSearchOptions();
        }
        [Given(@"I add Export Transaction name ""(.*)"" to Export")]
        public void GivenIAddExportTransactionNameToExport(string name)
        {
            importExportPage.AddExportName(name);
        }

        [Given(@"I add all Export Columns to Export")]
        public void GivenIAddAllExportColumnsToExport()
        {
            importExportPage.AddExportColumns();
            importExportPage.AddProfitCenters();
            importExportPage.ScrollDownToSearchOptions();
            importExportPage.AddTransactionTypes();
            importExportPage.AddSearchOption();
        }

        [Given(@"I click Run Export")]
        public void GivenIClickRunExport()
        {
            importExportPage.ClickRunExport();
        }

        [Given(@"I select Schedule from the Run Option Schedule")]
        public void GivenISelectScheduleFromTheRunOptionSchedule()
        {
            importExportPage.SelectRunAndDDOptionsArrow();
            importExportPage.SelectRunOptionSchedule();
        }


        [When(@"I search for ""(.*)"" on the search panel")]
        public void GivenISearchForOnTheSearchPanel(string name)
        {
            importExportPage.ClickSearchIcon();
            importExportPage.SearchExportName(name);
        }

        [Then(@"I can see the newly added ""(.*)"" Last Run Date is Today")]
        public void ThenICanSeeTheNewlyAddLastRunDateIsToday(string name)
        {
            importExportPage.LastRunDateIsToday();
        }

        [Then(@"I click on Properties and Export Results")]
        public void ThenIClickOnPropertiesAndExportResults()
        {
            importExportPage.ClickProperties();
            importExportPage.ClickExportResults();
        }

        [Then(@"I can see my ""(.*)"" in Export Results with Todays date")]
        public void ThenICanSeeMyInExportResultsWithTodaysDate(string name)
        {
            importExportPage.ExportResultsDisplaysName(name);
            importExportPage.ExportResultsLastRunDateIsToday();
        }

        [Then(@"I delete my ""(.*)"" from Transaction Data Exports")]
        public void ThenIDeleteMyFromTransactionDataExports(string p0)
        {
            importExportPage.ClickCloseExportWindow();
            importExportPage.ClickDelteExport();
        }

        [Given(@"I perform the Azure Service Bus Authentication")]
        public void GivenIPerformTheAzureServiceBusAuthentication()
        {
            //var stringJson = "{\"grant_type\":\"client_credentials\",\"client_id\":\"592aa7ce-8c1b-4524-af64-7edbd6f89dbf\",\"client_secret\":\"B-H~64q--GcJTmh.RM10GbC_F3diVsV51g\"}";
            var client = new RestClient("https://login.microsoftonline.com");
            var request = new RestRequest("/04cea7b8-f6d7-4a7e-bb33-e83a289b670b/oauth2/token", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            // request.AddHeader("Cookie", "fpc=AkHh2cjIhOJNnWRcj1JQpdcHKI-DAQAAAKU7p9gOAAAA; stsservicecookie=estsfd; x-ms-gateway-slice=estsfd");
            request.AddParameter("grant_type", "client_credentials", ParameterType.GetOrPost);
            request.AddParameter("client_id", "92aa7ce-8c1b-4524-af64-7edbd6f89dbf", ParameterType.GetOrPost);
            request.AddParameter("client_secret", "B-H~64q--GcJTmh.RM10GbC_F3diVsV51g", ParameterType.GetOrPost);
            request.AddParameter("resource", "https://servicebus.azure.net", ParameterType.GetOrPost);
            var response = client.Execute(request);
            JObject obs = JObject.Parse(response.Content);
        }

        [Given(@"I perform the Azure Service Bus Authentication with the following body")]
        public void GivenIPerformTheAzureServiceBusAuthenticationWithTheFollowingBody(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            var body = new ClientDetails
            {
                grant_type = (string)data.grant_type,
                client_id = (string)data.client_id,
                client_secret = (string)data.client_secret,
                resource = (string)data.resource
            };
            var client = new RestClient("https://login.microsoftonline.com");
            var request = new RestRequest("/04cea7b8-f6d7-4a7e-bb33-e83a289b670b/oauth2/token", Method.GET);
        //    request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(body);
            var response = client.Execute(request);
           // JObject obs = JObject.Parse(response.Content);
        }









    }
}


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CPOSAutomationFramework.Model
{
    class ClientDetails
    {
        public string grant_type { get; set;}
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public string resource { get; set; }
    }
}

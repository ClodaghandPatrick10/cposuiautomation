﻿using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;

namespace CPOSAutomationFramework.Settings
{
    public abstract class TestInitializeHook
    {
        public static void InitializeSettings()
        {
            //Set all the settings for framework
            ConfigReader.SetFrameworkSettings();

            //Open Browser
            OpenBrowser(TestSettings.BrowserType);

        }

        private static void OpenBrowser(BrowserType browserType = BrowserType.Chrome)
        {
            switch (browserType)
            {
                case BrowserType.InternetExplorer:
                    DriverContext.Driver = new InternetExplorerDriver();
                    DriverContext.Browser = new Browser(DriverContext.Driver);
                    break;
                //case BrowserType.FireFox:
                //    var binary = new FirefoxBinary(@"C:\Program Files (x86)\Mozilla Firefox\firefox.exe");
                //    var profile = new FirefoxProfile();
                //    DriverContext.Driver = new FirefoxDriver(binary, profile);
                //    DriverContext.Browser = new Browser(DriverContext.Driver);
                //    break;
                case BrowserType.Chrome:
                    DriverContext.Driver = new ChromeDriver();
                    DriverContext.Browser = new Browser(DriverContext.Driver);
                    break;
            }

        }

        //public virtual void NaviateSite()
        //{
        //    DriverContext.Browser.GoToUrl(TestSettings.AUT);
        //}



    }
}

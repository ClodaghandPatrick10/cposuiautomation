﻿using System;
using System.Xml.XPath;
using System.IO;

namespace CPOSAutomationFramework.Settings
{
    public class ConfigReader
    {

        public static void SetFrameworkSettings()
        {

            XPathItem appConnection;
            XPathItem browsertype;
            XPathItem aut;

            string strFilename = @"C:\Users\Nicola.Kehoe\source\repos\CPOS-UI-Automation\CPOSAutomation\Settings\GlobalConfig.xml";
            FileStream stream = new FileStream(strFilename, FileMode.Open);
            XPathDocument document = new XPathDocument(stream);
            XPathNavigator navigator = document.CreateNavigator();

            //Get XML Details and pass it in XPathItem type variables
            appConnection = navigator.SelectSingleNode("CPOSAutomationFramework/RunSettings/ApplicationDb");
            browsertype = navigator.SelectSingleNode("CPOSAutomationFramework/RunSettings/Browser");
            aut = navigator.SelectSingleNode("CPOSAutomationFramework/RunSettings/AUT");
            
            //Set XML Details in the property to be used accross framework
            TestSettings.AppConnectionString = appConnection.Value.ToString();
            TestSettings.AUT = aut.Value.ToString();
            TestSettings.BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), browsertype.Value.ToString());



        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace CPOSAutomationFramework.Settings
{
    public class BaseStep : Base
    {

        public virtual void NaviateToSite()
        {

                DriverContext.Browser.GoToUrl(TestSettings.AUT);

        }
    }
}

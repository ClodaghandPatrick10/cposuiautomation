﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
namespace CPOSAutomationFramework.Settings
{
    public class Base
    {
        public BasePage CurrentPage
        {
            get => (BasePage)ScenarioContext.Current["currentPage"];
            set => ScenarioContext.Current["currentPage"] = value;
        }

        private IWebDriver _driver { get; set; }
        //    public class Base
        //{
        //    public IWebDriver driver;
        //    [OneTimeSetUp]
        //    public void BeforeScenario()
        //    {
        //        driver = new ChromeDriver();
        //    }

        //    [OneTimeTearDown]
        //    public void AfterScenario()
        //    {
        //        driver.Quit();
        //    }
        //{
        //    public BasePage CurrentPage
        //    {
        //        get => (BasePage)ScenarioContext.Current["currentPage"];
        //        set => ScenarioContext.Current["currentPage"] = value;
        //    }

        protected TPage GetInstance<TPage>() where TPage : BasePage, new()
        {
            return (TPage)Activator.CreateInstance(typeof(TPage));
        }

        public TPage As<TPage>() where TPage : BasePage
        {
            return (TPage)this;
        }
    }

}
